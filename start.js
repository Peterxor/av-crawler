import { scrapeHtml, parseHtml, videoToDb } from './service'
import { chromeRobot, logger, mysqlMan } from './lib'
import to from 'await-to-js'
import _ from 'lodash'
import config from './config'
import enums from './enums'
const start = async () => {
  logger.info('start hello world')

  // mysqlMan.init()

  let [connErr, conn] = await to(mysqlMan.getConnection())
  if (connErr) {
    logger.error(connErr)
    return Promise.reject(connErr)
  }

  let [queryErr, result] = await to(conn.execute('show tables'))
  if (queryErr) {
    logger.error(queryErr)
    conn.release()
    return
  }
  console.log('rows: ', result[0])


  const [browser] = await chromeRobot.init()
  const page = await browser.newPage()
  await page.setDefaultNavigationTimeout(0)
  const pageLimit = config.urlSetting.pageLimit

  console.log('pagelimit: ', pageLimit, ' pagelimit type', typeof pageLimit)

  const avUrl = config.urlSetting.entry
  let pageNotFinish = true
  let actressUrl = []

  for (let actPage = 1; actPage < 2 && pageNotFinish ; actPage++) {
    // 先抓每一頁演員的連結
    logger.info('scraping ' + avUrl + '/' + actPage)
    const [scrapeErr, scrape] = await to(
        scrapeHtml(page, avUrl + '/' + actPage))
    if (scrapeErr) {
      logger.error('scrape error: ' + JSON.stringify(scrapeErr))
    } else {
      const [parseErr, parseData] = await to(parseHtml(scrape, enums.videoIdol))

      if (parseErr) {
        logger.error('parse error: ' + JSON.stringify(parseErr))
        pageNotFinish = false
      } else {
        actressUrl = _.concat(actressUrl, parseData)
      }
    }
  }

  console.log('actress number:', actressUrl.length)

  // let pageNotFinish = true
  // logger.info('start url: ' + avUrl)
  let videos = []
  pageNotFinish = true
  for (let actressIndex = 0; actressIndex < 1; actressIndex++) {
    for (let javPage = 1; javPage < 2 && pageNotFinish ; javPage++) {
      // 先抓每一頁的連結
      logger.info('scraping ' + actressUrl[actressIndex] + '/' + javPage)
      const [scrapeErr, scrape] = await to(
          scrapeHtml(page, actressUrl[actressIndex] + '/' + javPage))
      if (scrapeErr) {
        logger.error('scrape error: ' + JSON.stringify(scrapeErr))
      } else {
        const [parseErr, parseData] = await to(parseHtml(scrape, enums.videoUrl))

        if (parseErr) {
          logger.error('parse error: ' + JSON.stringify(parseErr))
          pageNotFinish = false
        } else {
          videos = _.concat(videos, parseData)
        }
      }
    }
  }
  videos = _.uniq(videos)
  // 解析每一個影片，並存入DB
  let tempVideo = []

  for (let i = 0; i < videos.length; i++) {
    logger.info('scraping ' + videos[i].videoUrl)
    const [scrapeErr, scrape] = await to(scrapeHtml(page, videos[i].videoUrl))
    if (scrapeErr) {
      logger.error('scrape error: ' + JSON.stringify(scrapeErr))
    } else {
      const [parseErr, parseData] = await to(parseHtml(scrape, enums.videoData))
      if (parseErr) {
        logger.error('parse error: ' + JSON.stringify(parseErr))
      } else {
        parseData.smallImage = videos[i].sImgUrl
        logger.info('parse ' + (i + 1) + 'th videoUrl ' + videos[i].videoUrl + ' done')
        tempVideo.push(parseData)
      }
    }
  }


  logger.info('crawler finish')

  logger.info('start db work2')

  for (let i = 0; i < tempVideo.length; i++) {
    logger.info(tempVideo[i].code + ' start insert to db')
    const [toDbError] = await to(videoToDb(conn, tempVideo[i]))
    if (toDbError) {
      logger.error(toDbError)
    } else {
      logger.info(tempVideo[i].code + ' insert to db success')
    }
  }
  conn.release()

  return Promise.resolve()
}

export default start
