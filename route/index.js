import { Router as experssRouter } from 'express'
import { easyToken } from '../middleware'
import { startCrawler, httpAgentTest } from '../api'
// import tokenVerify from '../middleware/token-verify'
// import api from '../api'
// import cors from 'cors'

const router = experssRouter()
router.route('/test').all(easyToken).get((req, res) => {
  res.status('200').send({
    code: '200',
    message: 'ok',
  })
})

router.route('/http-agent-test').post(httpAgentTest)

router.route('/start-crawler').all(easyToken).post(startCrawler)
export default router
