import to from 'await-to-js'
import cron from 'node-schedule'
import crawler from '../crawler'

const startCrawler = async () => {

  const crawlerTime = '0 0 23 * * *'

  cron.scheduleJob(crawlerTime, async () => crawler())

  return Promise.resolve()
}

export default startCrawler
