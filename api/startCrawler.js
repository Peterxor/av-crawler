// import to from 'await-to-js'
import crawler from '../crawler'

const startCrawler = async (req, res) => {
  // const [err] = await to(crawler())
  const result = crawler()
  return res.json({
    status: 200,
    message: 'start crawler'
  })
}

export default startCrawler
