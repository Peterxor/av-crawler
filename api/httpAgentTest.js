import to from 'await-to-js'
import { logger, httpAgent } from '../lib'
import { getCrawlerData } from '../service'
import enums from '../enums'



const httpAgentTest = async (req, res) => {
  const body = req.body
  let err
  let response
  // console.log(body)

  switch (body.httpWay) {
    case 'get':
      [err, response] = await to(httpAgent.get(body.url, body.query))
      break
    case 'post':
      [err, response] = await to(httpAgent.post(body.url, body.data))
      break
    case 'getCrawlerData':
      [err, response] = await to(getCrawlerData(enums.TAG))
      break
    default:
      break
  }

  if (err) {
    res.status(404).send(err.message)
    return
  }

  if (body.httpWay === 'getCrawlerData') {
    res.status(200).send(response)
    return
  }


  res.status(200).send(response.body)
}

export default httpAgentTest
