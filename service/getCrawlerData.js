import to from 'await-to-js'
import _ from 'lodash'
import config from '../config'
import { logger, httpAgent } from '../lib'

const getCrawlerData = async (dataType) => {
  const baseUrl = config.liveApi.site
  const take = config.liveApi.take
  let isNotFinish = true
  let page = 1
  let err = null
  let response
  let tempResult = []
  let trueResult = {}

  for (; isNotFinish; ){
    [err, response] = await to(httpAgent.get(`${baseUrl}/video/crawler`, {
      type: dataType,
      page,
      take
    }))
    if (err) {
      logger.error(`get all video code error, reason: ${err.message}`)
      break
    }
    page++
    if (response.body.result) {
      tempResult = _.concat(tempResult, response.body.result)
      console.log(tempResult)
    } else {
      console.log(response.body.result)
      isNotFinish = false
    }
  }
  if (err) {
    return Promise.reject(err)
  }

  for (let r of tempResult) {
    trueResult[r] = 1
  }
  return Promise.resolve(trueResult)

}

export default getCrawlerData
