import _ from 'lodash'
import to from 'await-to-js'
import {logger} from '../lib'

const videoToDb = async (conn, data) => {

  const [idErr, idolIds] = await to(getIdolId(conn, data))
  if (idErr) {
    return Promise.reject(idErr)
  }


  const [tagErr, tagIds] = await to(getTagId(conn, data))
  if (tagErr) {
    return Promise.reject(tagErr)
  }

  const [insertErr, insertId] = await to(insertVideo(conn, data))
  if (insertErr) {
    return Promise.reject(insertErr)
  }


  const [makeIdolRelationError] = await to(makeIdolRelation(conn, idolIds, insertId))
  if (makeIdolRelationError) {
    return Promise.reject(makeIdolRelationError)
  }

  const [makeTagRelationError] = await to(makeTagRelation(conn, tagIds, insertId))
  if (makeTagRelationError) {
    return Promise.reject(makeTagRelationError)
  }
  return Promise.resolve()
}

const makeIdolRelation = async (conn, idolIds, videoId) => {
  let idolVideoRelations = []
  _.forEach(idolIds, (idolId, key) => {
    idolVideoRelations.push([idolId, videoId])
  })


  const [insertIdolRelationErr, insertIdolRelation] = await to(
      conn.query('insert into video_has_idol (idol_id, video_id) values ?',
          [idolVideoRelations]))
  if (insertIdolRelationErr) {
    logger.error(insertIdolRelationErr)
    return Promise.reject(insertIdolRelationErr)
  }
  return Promise.resolve(insertIdolRelation)
}

const makeTagRelation = async (conn, tagIds, videoId) => {
  let tagVideoRelations = []
  _.forEach(tagIds, (tagId, key) => {
    tagVideoRelations.push([tagId, videoId])
  })

  const [insertTagRelationError, insertTagRelation] = await to(
      conn.query('insert into video_has_tag (tag_id, video_id) values ?',
          [tagVideoRelations]))
  if (insertTagRelationError) {
    logger.error(insertTagRelationError)
    return Promise.reject(insertTagRelationError)
  }
  return Promise.resolve(insertTagRelation)
}

const insertVideo = async (conn, data) => {
  const video = {
    code: data.code,
    title: data.title,
    date: data.date,
    video_time: data.period,
    producer: data.producer,
    seller: data.seller,
    big_image: data.bigImage,
    small_image: data.smallImage,
  }


  let videoId = null

  const [queryError, [rows]] = await to(
      conn.execute('select * from video_meta where video_meta.code = ?', [data.code]))
  if (queryError) {
    logger.error(queryError)
    return Promise.reject(queryError)
  }
  if (rows.length === 0) {
    const [insertError, result] = await to(
        conn.query('insert into video_meta set ?', video))
    if (insertError) {
      logger.error(insertError)
      return Promise.reject(insertError)
    }
    videoId = result[0].insertId
  } else {
    videoId = rows[0].id
  }
  return Promise.resolve(videoId)
}

const getIdolId = async (conn, data) => {
  const idols = data.idols
  const idolIds = []
  for (let i = 0; i < idols.length; i++) {
    // console.log(i, idols[i])
    const [queryError, [rows]] = await to(
        conn.execute('select * from idols where idols.name = ?', [idols[i]]))
    // console.log(rows)
    if (queryError) {
      logger.error(data.code + ' ' + idols[i] + ' idol select is error: ' +
          queryError.message)
    } else {
      if (rows.length > 0) {
        idolIds.push(rows[0].id)
      } else {
        const [insertError, insert] = await to(
            conn.execute('insert into idols set name = ?', [idols[i]]))
        if (insertError) {
          logger.error(insertError)
        } else {
          idolIds.push(insert[0].insertId)
        }
      }
    }
  }

  return Promise.resolve(idolIds)
}

const getTagId = async (conn, data) => {
  const tags = data.tags
  const tagIds = []
  for (let i = 0; i < tags.length; i++) {
    const [queryError, [rows]] = await to(
        conn.execute('select * from tags where tags.name = ?', [tags[i]]))
    if (queryError) {
      logger.error(data.code + ' ' + tags[i] + ' tag select is error: ' +
          queryError.message)
    } else {
      if (rows.length > 0) {
        // logger.info(data.code + ' ' + tags[i] + ' tag select is exist')
        // console.log(rows)
        tagIds.push(rows[0].id)
      } else {
        // logger.info('prepare to insert ' + tags[i])
        const [insertError, insert] = await to(
            conn.execute('insert into tags set name = ?', [tags[i]]))
        if (insertError) {
          logger.error(insertError)
        } else {
          // console.log('insert success', insert[0])
          tagIds.push(insert[0].insertId)
        }
      }
    }
  }
  return Promise.resolve(tagIds)
}

export default videoToDb
