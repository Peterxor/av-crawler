import cheerio from 'cheerio'
import _ from 'lodash'
import to from 'await-to-js'
import {mysqlMan, logger} from '../lib'
import enums from '../enums'

const parseHtml = async (scrape, parseClass) => {
  const $ = cheerio.load(scrape)
  switch (parseClass) {
    case enums.videoUrl:
      const [urlError, urls] = await to(getUrl($))
      if (urlError) {
        return Promise.reject(urlError)
      }
      return Promise.resolve(urls)
    case enums.videoData:
      const [dataError, data] = await to(getData($))
      if (dataError) {
        return Promise.reject(dataError)
      }
      return Promise.resolve(data)
    case enums.videoIdol:
      const [actressError, actress] = await to(getActressUrl($))
      if (actressError) {
        return Promise.reject(actressError)
      }
      return Promise.resolve(actress)
    default:
      const errMsg = 'parseClass doesnt exist, parseClass: ' +
          JSON.stringify(parseClass)
      logger.error(errMsg)
      return Promise.reject(errMsg)
  }
}

const getActressUrl = async ($) => {
  const actressUrl = $('a[class="avatar-box text-center"]')
  const actressUrls = []
  _.forEach(actressUrl, (url, key) => {
    // console.log($(url).attr('href'))
    actressUrls.push($(url).attr('href'))
  })
  return Promise.resolve(actressUrls)
}

const getData = async ($) => {
  const infos = $('div[class="col-md-3 info"] p')

  let videoCode = ''
  const videoData = {
    code: '',
    title: '',
    date: '',
    period: '',
    producer: '',
    seller: '',
    idols: [],
    tags: [],
  }

  // 抓影片資料
  _.forEach(infos, (info, key) => {
    if ($(info).find('span[style="color:#CC0000;"]').text()) {
      videoCode = $(info).find('span[style="color:#CC0000;"]').text()
      videoData.code = videoCode
      return
    }

    if ($(info).find('span[class="header"]').text() === '發行日期:') {
      $(info).find('span[class="header"]').remove()
      videoData.date = $(info).text().trim()
      return
    }

    if ($(info).find('span[class="header"]').text() === '長度:') {
      $(info).find('span[class="header"]').remove()
      videoData.period = $(info).text().trim()
      return
    }

    if ($(info).find('span[class="header"]').text() === '製作商:') {
      videoData.producer = $(info).find('a').text().trim()
      return
    }

    if ($(info).find('span[class="header"]').text() === '發行商:') {
      videoData.seller = $(info).find('a').text().trim()
      return
    }

    if ($(info).find('span[class="genre"]').text()) {
      const others = $(info).find('span[class="genre"]')
      if ($(others).attr('onmouseover')) {
        _.forEach(others, (other, index) => {
          videoData.idols.push($(other).find('a').text())
        })
      } else {
        _.forEach(others, (other, index) => {
          videoData.tags.push($(other).find('a').text())
        })
      }
    }
  })

  //抓影片標題
  const h3s = $('h3')
  _.forEach(h3s, (h3, key) => {
    const h3Text = $(h3).text()
    videoData.title = h3Text
  })

  //抓影片大圖連結
  const image = $('a[class="bigImage"]').attr('href')
  // console.log('bigImage:', image)

  videoData.bigImage = image

  // console.log(videoData)
  return Promise.resolve(videoData)
}

const getUrl = async ($) => {
  const titles = $('title')
  const avs = []
  let errMsg = null
  _.forEach(titles, (title, key) => {
    const titleText = $(title).text()
    if (titleText === '404 Page Not Found!') {
      errMsg = 'page finish'
    } else {
      const as = $('a')
      _.forEach(as, (a, key) => {
        const aClass = $(a).attr('class')
        if (aClass === 'movie-box') {
          const avUrl = $(a).attr('href')
          const imgSrc = $(a).find('div[class="photo-frame"] img').attr('src')

          avs.push({
            videoUrl: avUrl,
            sImgUrl: imgSrc,
          })
        }
      })
    }
  })
  if (errMsg) {
    return Promise.reject(errMsg)
  }
  return Promise.resolve(avs)
}

export default parseHtml
