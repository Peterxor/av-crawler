import to from 'await-to-js'


const scrapeHtml = async (page, url) => {

  const [pageErr] = await to(page.goto(url), { waitUntil: ['domcontentloaded'] })
  if (pageErr) {
    logger.error('puppeteer goto fail')
    return Promise.reject(pageErr)
  }
  // console.log(url, page.url())
  const [contentErr, content] = await to(page.content())
  if (contentErr) {
    logger.error('puppeteer get content fail')
    return Promise.reject(contentErr)
  }
  return Promise.resolve(content)
}

export default scrapeHtml
