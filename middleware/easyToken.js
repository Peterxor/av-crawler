import crypto from 'crypto'
import { logger } from '../lib'

const easyToken = (req, res, next) => {
  const { authorization } = req.headers
  if (!authorization) {
    const msg = 'no authorization'
    logger.error(msg)
    return res.status('405').send({
      code: '405',
      message: msg,
    })
  }

  // console.log(authorization)
  const auth = authorization.split(' ')

  const hash = crypto.createHash('sha256').update('aflix').digest('hex')
  // console.log(auth[1], hash)
  if (hash === auth[1]) {
    next()
  } else {
    const msg = 'wrong authorization'
    logger.error(msg)
    return res.status('405').send({
      code: '405',
      message: msg,
    })
  }
}
export default easyToken
