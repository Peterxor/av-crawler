import {chromeRobot, logger, mysqlMan} from '../lib'
import to from 'await-to-js'
import config from '../config'
import {parseHtml, scrapeHtml, videoToDb} from '../service'
import enums from '../enums'
import _ from 'lodash'

const crawler = async () => {
  logger.info('start hello world')

  // mysqlMan.init()

  // let [connErr, conn] = await to(mysqlMan.getConnection())
  // if (connErr) {
  //   logger.error(connErr)
  //   return Promise.reject(connErr)
  // }

  // let [queryErr, result] = await to(conn.execute('show tables'))
  // if (queryErr) {
  //   logger.error(queryErr)
  //   conn.release()
  //   return
  // }
  // console.log('rows: ', result[0])


  const [browser] = await chromeRobot.init()
  const page = await browser.newPage()
  await page.setDefaultNavigationTimeout(0)
  const pageLimit = config.urlSetting.pageLimit

  // console.log('pagelimit: ', pageLimit, ' pagelimit type', typeof pageLimit)

  const avUrl = config.urlSetting.entry
  let pageNotFinish = true
  let actressUrl = []

  for (let actPage = 1; actPage < 1000 && pageNotFinish ; actPage++) {
    // 先抓每一頁演員的連結
    logger.info('scraping idle url ' + avUrl + '/' + actPage)
    const [scrapeErr, scrape] = await to(
        scrapeHtml(page, avUrl + '/' + actPage))
    if (scrapeErr) {
      logger.error('scrape error: ' + JSON.stringify(scrapeErr))
    } else {
      const [parseErr, parseData] = await to(parseHtml(scrape, enums.videoIdol))

      if (parseErr) {
        logger.error('parse error: ' + JSON.stringify(parseErr))
        pageNotFinish = false
      } else {
        actressUrl = _.concat(actressUrl, parseData)
      }
    }
  }

  console.log('actress number:', actressUrl.length)

  // let pageNotFinish = true
  // logger.info('start url: ' + avUrl)
  let videos = []
  pageNotFinish = true
  for (let actressIndex = 0; actressIndex < actressUrl.length; actressIndex++) {
    console.log(`actressIndex: ${actressIndex}`)
    pageNotFinish = true
    for (let javPage = 1; javPage < pageLimit && pageNotFinish ; javPage++) {
      // 先抓每一頁的連結
      logger.info('scraping ' + actressUrl[actressIndex] + '/' + javPage)
      const [scrapeErr, scrape] = await to(
          scrapeHtml(page, actressUrl[actressIndex] + '/' + javPage))
      if (scrapeErr) {
        logger.error('scrape error: ' + JSON.stringify(scrapeErr))
      } else {
        const [parseErr, parseData] = await to(parseHtml(scrape, enums.videoUrl))

        if (parseErr) {
          logger.error('parse error: ' + JSON.stringify(parseErr))
          pageNotFinish = false
        } else {
          videos = _.concat(videos, parseData)
        }
        console.log(`video number: ${videos.length}`)
      }
    }
  }
  videos = _.uniq(videos)
  console.log(`video number: ${videos.length}`)
  return Promise.resolve()
  // 解析每一個影片，並存入DB
  let tempVideo = []

  for (let i = 0; i < videos.length; i++) {
    logger.info('scraping ' + videos[i].videoUrl)
    const [scrapeErr, scrape] = await to(scrapeHtml(page, videos[i].videoUrl))
    if (scrapeErr) {
      logger.error('scrape error: ' + JSON.stringify(scrapeErr))
    } else {
      const [parseErr, parseData] = await to(parseHtml(scrape, enums.videoData))
      if (parseErr) {
        logger.error('parse error: ' + JSON.stringify(parseErr))
      } else {
        parseData.smallImage = videos[i].sImgUrl
        logger.info('parse ' + (i + 1) + 'th videoUrl ' + videos[i].videoUrl + ' done')
        tempVideo.push(parseData)
      }
    }
  }


  logger.info('crawler finish')
  console.log(tempVideo)
  //
  // logger.info('start db work2')
  //
  // for (let i = 0; i < tempVideo.length; i++) {
  //   logger.info(tempVideo[i].code + ' start insert to db')
  //   const [toDbError] = await to(videoToDb(conn, tempVideo[i]))
  //   if (toDbError) {
  //     logger.error(toDbError)
  //   } else {
  //     logger.info(tempVideo[i].code + ' insert to db success')
  //   }
  // }
  // conn.release()

  return Promise.resolve()
}

export default crawler



// {
//   code: 'MDBK-089', title: 'MDBK-089 BAZOOKAが誇る超人気女優の極上SEXベスト30選',
//   date: '2020-02-28',
//   period: '241分鐘',
//   producer: 'メディアステーション',
//   seller: 'BAZOOKA（バズーカ）',
//   idols: [
//    'AIKA',  'あおいれな', '跡美しゅり', '阿部乃みく',
//    'なつめ愛莉', '並木杏梨',  '蓮実クレア', '波多野結衣',
//    '浜崎真緒',  '星奈あい',  '麻里梨夏',  '水野朝陽',
//    '美谷朱里',  '南梨央奈',  '三原ほのか', '高杉麻里',
//    '篠宮ゆり',  '篠田ゆう',  'あべみかこ', '大槻ひびき',
//    '推川ゆうり', '尾上若葉',  '佳苗るか',  '北川エリカ',
//    '霧島さくら', '倉多まお',  '枢木あおい', '紺野ひかる',
//    '桜ちなみ',  '宮崎あや'],
//   tags: [ '苗條', '巨乳', '合集', '高畫質', '4小時以上作品' ],
//   bigImage: 'https://pics.javbus.com/cover/7jyh_b.jpg',
//   smallImage: 'https://pics.javbus.com/thumb/7jyh.jpg'
// },
