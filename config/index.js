const config = {
  grayLogOpt: {
    connect: {
      host: process.env.GRAYLOG_HOST,
      port: process.env.GRAYLOG_PORT,
    },
    openGrayLog: process.env.OPEN_GRAYLOG * 1,
    grayLogLevel: process.env.GRAYLOG_LEVEL,
    showLogLevel: process.env.SHOW_LOG_LEVEL,
  },
  chromeRobotSet: {
    chromeTimeOut: process.env.CHROME_TIME_OUT,
    debugMode: process.env.DEBUG_MODE === 1,
  },
  urlSetting: {
    entry: process.env.ENTRY_WEBSITE,
    otherWebs: process.env.SCRAPE_OTHER_WEBSITES,
    pageLimit: process.env.WEB_PAGE_LIMIT,
  },
  mysqlSetting: {
    host: process.env.MYSQL_HOST,
    port: process.env.MYSQL_PORT,
    user: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASSWORD,
    database: process.env.MYSQL_DATABASE
  },
  httpSetting: {
    port: process.env.WEB_PORT
  },
  cronjobStart: process.env.CRON_JOB_AGREE,
  liveApi: {
    site: process.env.LIVE_API_SITE,
    take: process.env.LIVE_API_TAKE,
  }
}

for (const key in config) {
  if ({}.hasOwnProperty.call(config, key)) {
    exports[key] = config[key]
  }
}
