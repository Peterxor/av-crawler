// eslint-disable-next-line no-return-await
import { logger, mysqlMan, webController } from './lib'
// import to from 'await-to-js'
// import schedule from 'node-schedule'
// import start from './start'

(async () => {
  // eslint-disable-next-line global-require
  require('events').EventEmitter.defaultMaxListeners = Infinity
  process.setMaxListeners(0)
  process.on('uncaughtException', (err) => {
    logger.error(`uncaughtException got an error: ${err.message} ${err.stack}`)
    process.exit(0)
  })
  process.on('unhandledRejection', (err) => {
    logger.error(`unhandledRejection got an error: ${err.message} ${err.stack}`)
    process.exit(0)
  })

  webController.init()
  webController.createServer()






  // const [err] = await to(start())
  // if (err) {
  //   logger.error(err)
  // }

  // mysqlMan.init()
  //
  // let [connErr, conn] = await to(mysqlMan.getConnection())
  // if (connErr) {
  //   logger.error(connErr)
  //   return
  // }
  //
  // let values = [
  //     ['xxx'],
  //     ['yyy'],
  //     ['zzz'],
  // ]
  // let [queryErr, result] = await to(conn.query('insert into idols (name) values ?', [values]))
  // if (queryErr) {
  //   logger.error(queryErr)
  //   conn.release()
  //   return
  // }
  // console.log('rows: ', result[0])
  //
  // conn.release()
})()
