import _ from 'lodash'
import puppeteer from 'puppeteer'
import { logger } from '.'
import { chromeRobotSet } from '../config'


const sample = [
  // '--incognito',
  '--disable-features=site-per-process',
  '--no-sandbox',
  '--disable-setuid-sandbox',
  '--ignore-certificate-errors',
]

const chromeRobot = {
  init: async () => {
    const args = _.clone(sample)

    const config = {
      args,
      timeout: chromeRobotSet.chromeTimeOut,
    }

    if (chromeRobotSet.debugMode) {
      config.devtool = 1
    } else {
      config.devtool = 0
    }

    const browser = await puppeteer.launch(config)
    return [browser]
  },
}

export default chromeRobot
