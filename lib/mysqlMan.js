import database from 'mysql2'
import { logger } from '.'
import to from 'await-to-js'
import config from '../config'

let pool = {}

const mysqlMan = {
  init: async () => {
    pool = database.createPool({
      host: config.mysqlSetting.host,
      port: config.mysqlSetting.port,
      user: config.mysqlSetting.user,
      password: config.mysqlSetting.password,
      database: config.mysqlSetting.database,
      waitForConnections: true,
      connectionLimit: 10,
      queueLimit: 0,
    })

    pool.on('acquire', (connection) => {
      // logger.info(`User: ${i[3]} Connection acquired', ${connection.threadId}`)
    })
    pool.on('connection', (connection) => {
      logger.info(`db connected`)
      connection.query('SET SESSION auto_increment_increment=1')
    })

    pool.on('enqueue', () => {
      logger.info(`User: Waiting for available connection slot`)
    })
    pool.on('release', (connection) => {
      logger.info(`User:  Connection released', ${connection.threadId}`)
    })
  },

  getConnection: async () => {
    const [err, conn] = await to(pool.promise().getConnection())
    if (err) {
      return Promise.reject(err)
    }
    return Promise.resolve(conn)
  },
}

export default mysqlMan
