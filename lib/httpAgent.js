import superAgent from 'superagent'
import to from 'await-to-js'
import { logger } from '.'


let agentCore = null

const httpAgent = {
  init: async () => {
    agentCore = superAgent
    return Promise.resolve()
  },
  get: async (url, query) => {
    if (agentCore === null) {
      agentCore = superAgent
    }
    const [err, res] = await to(agentCore.get(url).query(query))
    if (err) {
      logger.error(`http get request error, requset: ${JSON.stringify({url, query})}, reason: ${err}`)
      return Promise.reject(err)
    }
    return Promise.resolve(res)
  },
  post: async (url, data) => {
    if (agentCore === null) {
      agentCore = superAgent
    }
    const [err, res] = await to(agentCore.post(url).send(data))
    if (err) {
      logger.error(`http post request error, request: ${JSON.stringify({url, data})}, reason: ${err}`)
      return Promise.reject(err)
    }
    return Promise.resolve(res)
  }
}

export default httpAgent
