import _ from 'lodash'
import WinstonGraylog2 from 'winston-graylog2'
import { createLogger, format, transports } from 'winston'
// import { grayLogOpt } from '../config'
// import { SPLAT } from 'triple-beam'

let logger = null
if (!logger) {
  const workerId = process.env.pm_id
  const colorizer = format.colorize()
  const transportsArr = [
    new transports.Console({
      // level: grayLogOpt.showLogLevel || 'silly',
      level: 'silly',
      format: format.combine(
          format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
          format.splat(),
          format.printf(
              (msg) => {
                if (_.isObject(msg.message)) {
                  msg.message = JSON.stringify(msg.message)
                }
                const { level, message, timestamp } = msg
                return colorizer.colorize(
                    level,
                    `[WORKER-${workerId} ${timestamp} ] - ${(message)}\n`,
                )
              },
          ),
      ),
    }),
    new transports.File({
      level: 'error',
      filename: 'logs/winston-error.log',
      format: format.combine(
          format.timestamp({
            format: 'YYYY-MM-DD HH:mm:ss',
          }),
          format.printf(
              (info) => {
                if (_.isObject(info.message)) {
                  info.message = JSON.stringify(info.message)
                }
                const { level, message, timestamp } = info
                return `[ WORKER ${workerId} ${level.toUpperCase()} ${timestamp} ] - ${(message)} \n`
              },
          ),
      ),
    }),
      new transports.File({
        level: 'info',
        filename: 'logs/winston-info.log',
        format: format.combine(
            format.timestamp({
              format: 'YYYY-MM-DD HH:mm:ss',
            }),
            format.printf(
                (info) => {
                  if (_.isObject(info.message)) {
                    info.message = JSON.stringify(info.message)
                  }
                  const { level, message, timestamp } = info
                  return `[ WORKER ${workerId} ${level.toUpperCase()} ${timestamp} ] - ${(message)} \n`
                },
            ),
        ),
      })
  ]
  // if (grayLogOpt.openGrayLog) {
  //   transportsArr.push(
  //       new WinstonGraylog2({
  //         name: 'Graylog',
  //         level: grayLogOpt.grayLogLevel || 'error',
  //         silent: false,
  //         handleExceptions: true,
  //         graylog: {
  //           servers: [grayLogOpt.connect],
  //           facility: process.env.name,
  //           bufferSize: 1400,
  //         },
  //       }),
  //   )
  // }

  logger = createLogger({
    transports: transportsArr,
    exitOnError: false,
  })
  const errorFunc = logger.error
  logger.error = (...args) => {
    args = _.map(args, arg => (_.isError(arg) ? arg.message : arg))
    errorFunc(...args)
  }

  const infoFunc = logger.info
  logger.info = (...args) => {
    args = _.map(args, arg => (_.isError(arg) ? arg.message : arg))
    infoFunc(...args)
  }


}
export default logger
