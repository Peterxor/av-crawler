import _ from 'lodash'
import to from 'await-to-js'
import path from 'path'
import debug from 'debug'
import multer from 'multer'
import express from 'express'
import cookieParser from 'cookie-parser'
import router from '../route'
import cronjob from '../cronjob'
import config from '../config'
import http from 'http'
import crawler from '../crawler'
import { logger as winston, chromeRobot, mysqlMan, httpAgent } from '.'

let app = null
let server = null

const webController = {
  init: async () => {
    const logger = require('morgan')

    debug('client:server')
    app = express()
    const upload = multer()

    app.use(express.json({limit: '30mb'}))
    app.use(express.urlencoded({extended: true}))
    app.use(upload.array())
    app.use(cookieParser())
    app.use(express.static(path.join(__dirname, 'public')))
    app.set('views', path.join(__dirname, 'views'))
    app.set('view engine', 'pug')
    app.use(logger('dev'))

    app.use('/', router)

    app.use((err, req, res) => {
      // set locals, only providing error in development
      res.locals.message = err.message
      res.locals.error = req.app.get('env') === 'development' ? err : {}

      // render the error page
      res.status(err.status || 500)
      res.render('error')
    })
    app.set('port', config.httpSetting.port)
  },
  createServer: () => {
    server = http.createServer(app)
    const port = config.httpSetting.port

    winston.verbose(`
=============================================
===> APP run in ${process.env.NODE_ENV} listen on ${port}
=============================================
`)

    server.listen(port)
    server.on('error', (error) => {
      const bind = typeof port === 'string'
          ? `Pipe ${port}`
          : `Port ${port}`

      // handle specific listen errors with friendly messages
      switch (error.code) {
        case 'EACCES':
          winston.error(`${bind} requires elevated privileges`)
          process.exit(1)
          break
        case 'EADDRINUSE':
          winston.error(`${bind} is already in use`)
          process.exit(1)
          break
        default:
          throw error
      }

      if (error.syscall !== 'listen') {
        throw error
      }
    })

    server.on('listening', async () => {
      const addr = server.address()
      const bind = typeof addr === 'string'
          ? `pipe ${addr}`
          : `port ${addr.port}`
      debug(`Listening on ${bind}`)

      // await mysqlMan.init()
      await httpAgent.init()
      await crawler()

      // console.log(typeof config.cronjobStart, config.cronjobStart)
      if (config.cronjobStart * 1 === 1) {
        _.forEach(cronjob, async (func, name) => {
          const [functionError] = await to(func())
          if (functionError) {
            winston.error(`${name} : ${_.isError(functionError) ?
                functionError.stack :
                functionError}`)
          }
        })
      }
    })
  }
}

export default webController



